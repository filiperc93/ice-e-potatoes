=== getting started ===

pip3 install face_recognition opencv-python
apt  install libqtgui4

=== exemplo de reconhecimento facial na webcam ===

cd glauco
python3 facerec_from_webcam_faster.py
tecla "q" para sair

=== Adicionar este remote ===
git remote add origin https://gitlab.com/glaucocarvalho/ice-e-potatoes.git

=== Como colaborar ===

* Em sua máquina crie um virtualenv com os parâmetros -python3 --always-copy
* entre no diretório deste virtualenv
* git clone https://gitlab.com/glaucocarvalho/ice-e-potatoes
* Navegue para sua respectiva pasta de colaborador e trabalhe nela

Não altere o trabalho dos amiguinhos sem o consentimento prévio!

* faça commits separados de acordo com a responsabilidade de cada tarefa
* Ao final de seu trabalho git push origin --all e use suas credenciais conforme forem pedidas
** (duvidas) veja "Adicionar este remote"
